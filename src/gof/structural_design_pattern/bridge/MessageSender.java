package gof.structural_design_pattern.bridge;

public interface MessageSender {
    void sendMessage();
}
