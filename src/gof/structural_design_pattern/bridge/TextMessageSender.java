package gof.structural_design_pattern.bridge;

public class TextMessageSender implements MessageSender{
    @Override
    public void sendMessage() {
        System.out.println("Sending text message.");
    }
}
