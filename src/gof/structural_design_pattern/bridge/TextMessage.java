package gof.structural_design_pattern.bridge;

public class TextMessage extends Message{

    public TextMessage(MessageSender messageSender) {
        super(messageSender);
    }
    @Override
    public void send() {
        messageSender.sendMessage();
    }
}
