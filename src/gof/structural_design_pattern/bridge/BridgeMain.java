package gof.structural_design_pattern.bridge;

public class BridgeMain {
    public static void main(String[] args) {
        MessageSender textMessageSender = new TextMessageSender();
        Message messageText = new TextMessage(textMessageSender);
        messageText.send();

        MessageSender emailMessageSender = new EmailMessageSender();
        Message messageEmail = new EmailMessage(emailMessageSender);
        messageEmail.send();
    }
}
