package gof.structural_design_pattern.bridge;

public class EmailMessageSender implements MessageSender{
    @Override
    public void sendMessage() {
        System.out.println("Sending email message");
    }
}
