package gof.structural_design_pattern.facade;

public class OrderServiceImpl implements OrderService{

    @Override
    public boolean placeOrder(int productId) {
        Product product = new Product();
        product.productId = productId;
        boolean orderDone = false;
        if(InventoryService.isAvailable(product)) {
            System.out.println("Product " + product.productId + " is available");
            PaymentService.makePayment();
            System.out.println("Payment is done");
            orderDone = true;
        }
        return orderDone;
    }
}
