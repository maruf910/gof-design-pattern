package gof.structural_design_pattern.facade;

public class PaymentService {
    public static boolean makePayment() {
        return true;
    }
}
