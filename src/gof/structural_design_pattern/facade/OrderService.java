package gof.structural_design_pattern.facade;

public interface OrderService {
    boolean placeOrder(int productId);
}
