package gof.structural_design_pattern.facade;

public class Product {
    public int productId;
    public String name;
    public Product(){}
    public Product(int id,String name) {
        this.productId = id;
        this.name = name;
    }
}
