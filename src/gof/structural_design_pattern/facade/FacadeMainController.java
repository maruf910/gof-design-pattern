package gof.structural_design_pattern.facade;

public class FacadeMainController {
    public static void main(String[] args) {
        OrderService orderService = new OrderServiceImpl();
        boolean orderStatus = orderService.placeOrder(10);
        if(orderStatus) {
            System.out.println("Order placed");
        } else {
            System.out.println("Order not placed");
        }

    }
}
