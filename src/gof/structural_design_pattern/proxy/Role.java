package gof.structural_design_pattern.proxy;

public class Role {
    private String role;
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
}
