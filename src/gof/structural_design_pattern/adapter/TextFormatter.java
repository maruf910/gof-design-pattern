package gof.structural_design_pattern.adapter;

public class TextFormatter implements TextFormattable{

    @Override
    public String formatText(String text) {
        return text.replace(".","\n");
    }
}
