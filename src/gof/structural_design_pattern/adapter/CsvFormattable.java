package gof.structural_design_pattern.adapter;

public interface CsvFormattable {
    String formatText(String text);
}
