package gof.structural_design_pattern.adapter;

public class CsvAdapterImpl implements TextFormattable{
    CsvFormattable csvFormatter;
    public CsvAdapterImpl(CsvFormattable csvFormatter) {
        this.csvFormatter = csvFormatter;
    }
    @Override
    public String formatText(String text) {
        String retText = csvFormatter.formatText(text);
        return retText;
    }
}
