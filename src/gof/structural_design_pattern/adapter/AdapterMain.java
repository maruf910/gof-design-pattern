package gof.structural_design_pattern.adapter;

public class AdapterMain {
    public static void main(String[] args) {
        String testText = "Line 1.Line 2.Line 3.";
        TextFormattable textFomatter = new TextFormatter();
        String res = textFomatter.formatText(testText);
        System.out.println(res);

        CsvFormattable csvFormatter = new CsvFormatter();
        CsvAdapterImpl csvAdapter = new CsvAdapterImpl(csvFormatter);
        String csvRes = csvAdapter.formatText(testText);
        System.out.println(csvRes);
    }
}
