package gof.structural_design_pattern.adapter;

public class CsvFormatter implements CsvFormattable{
    @Override
    public String formatText(String text) {
        return text.replace(".",",");
    }
}
