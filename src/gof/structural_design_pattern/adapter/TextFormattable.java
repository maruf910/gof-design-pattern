package gof.structural_design_pattern.adapter;

public interface TextFormattable {
    String formatText(String text);
}
