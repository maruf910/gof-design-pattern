package gof.structural_design_pattern.decorator;

public abstract class FlowerBouquetDecorator extends FlowerBouquet{
    public abstract String getDescription();
}
