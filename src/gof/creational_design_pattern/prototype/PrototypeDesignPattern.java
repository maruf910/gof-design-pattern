package gof.creational_design_pattern.prototype;

import java.util.ArrayList;
import java.util.List;

public class PrototypeDesignPattern {

    public static void main(String[] args) {
        List<Shape> shapeList = new ArrayList<>();
        Circle circle = new Circle();
        circle.x = 10;
        circle.y = 5;
        circle.radius = 3;
        circle.color = "blue";
        shapeList.add(circle);

        Rectangle rectangle = new Rectangle();
        rectangle.height = 2;
        rectangle.x = 25;
        rectangle.y = 35;
        rectangle.color = "white";
        shapeList.add(rectangle);
        cloneAndCompare(shapeList);
    }

    public static void cloneAndCompare(List<Shape> shapeList) {
        List<Shape> clonedShape = new ArrayList<>();

        for(Shape shape: shapeList) {
            clonedShape.add(shape.clone());
        }

        for(int i=0;i<shapeList.size();i++) {
            if(shapeList.get(i) == clonedShape.get(i)) {
                System.out.println(i + " same object");
            } else {
                System.out.println(i + " different object");
                if(shapeList.get(i).equals(clonedShape.get(i))) {
                    System.out.println(i + " shapes are identical");
                } else {
                    System.out.println(i + " shapes are not identical");
                }
            }
        }
    }
}
