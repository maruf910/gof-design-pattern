package gof.creational_design_pattern.prototype;

import java.util.Objects;

public class Rectangle extends Shape{
    public int height;

    public Rectangle(){};

    public Rectangle(Rectangle target) {
        super(target);
        if(target!=null) {
            this.height = target.height;
        }
    }
    @Override
    public Shape clone() {
        return new Rectangle(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rectangle)) return false;
        if (!super.equals(o)) return false;
        Rectangle rectangle = (Rectangle) o;
        return height == rectangle.height;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), height);
    }
}
