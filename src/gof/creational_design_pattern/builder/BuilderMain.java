package gof.creational_design_pattern.builder;

public class BuilderMain {
    public static void main(String[] args) {
        User user1= new User.UserBuilder("Sumaiya", "Tasneem")
                .age(25)
                .address("Khalifa city,Abu dhabi")
                .phone("0563456277")
                .build();
        System.out.println(user1);
        User user2= new User.UserBuilder("Nusaiba binte", "Maruf")
                .age(2)
                .build();
        System.out.println(user2);
    }
}
