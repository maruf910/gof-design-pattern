package gof.creational_design_pattern.abstract_factory;

public interface Cheese {
    void prepareCheese();
}
