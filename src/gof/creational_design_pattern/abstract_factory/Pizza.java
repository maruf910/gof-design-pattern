package gof.creational_design_pattern.abstract_factory;

public abstract class Pizza {
    public abstract void addIngredients();
}
