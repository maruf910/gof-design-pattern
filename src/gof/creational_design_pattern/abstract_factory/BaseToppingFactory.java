package gof.creational_design_pattern.abstract_factory;

public abstract class BaseToppingFactory {
    public abstract Cheese createCheese();
}
