package gof.creational_design_pattern.abstract_factory;

public class GoatCheese implements Cheese{
    public  GoatCheese(){
        prepareCheese();
    }
    @Override
    public void prepareCheese(){
        System.out.println("Preparing goat cheese...");
    }
}
