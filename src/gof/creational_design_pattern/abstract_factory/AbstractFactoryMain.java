package gof.creational_design_pattern.abstract_factory;

public class AbstractFactoryMain {
    public static void main(String[] args) {
        Pizza cheesePizza = GourmetPizzaFactory.createPizza("cheese");
        Pizza vegPizza = GourmetPizzaFactory.createPizza("veggie");

        Pizza cheesePizza2 = SicilianPizzaFactory.createPizza("cheese");
        Pizza vegPizza2 = SicilianPizzaFactory.createPizza("veggie");
    }
}
