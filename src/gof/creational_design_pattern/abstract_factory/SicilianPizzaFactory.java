package gof.creational_design_pattern.abstract_factory;

public class SicilianPizzaFactory {
    public static Pizza createPizza(String type){
        Pizza pizza;
        BaseToppingFactory toppingFactory= new SicilianToppingFactory();
        switch (type.toLowerCase())
        {
            case "cheese":
                pizza = new CheesePizza(toppingFactory);
                break;
            case "veggie":
                pizza = new VeggiePizza(toppingFactory);
                break;
            default: throw new IllegalArgumentException("No such pizza.");
        }
        pizza.addIngredients();
        return pizza;
    }
}
