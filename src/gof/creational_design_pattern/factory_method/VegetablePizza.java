package gof.creational_design_pattern.factory_method;

public class VegetablePizza implements Pizza{
    @Override
    public void addIngredients() {
        System.out.println("Adding ingredients for vegetable pizza");
    }

    @Override
    public void bakePizza() {
        System.out.println("Bake vegetable pizza");
    }
}
