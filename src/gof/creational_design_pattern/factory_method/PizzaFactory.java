package gof.creational_design_pattern.factory_method;

public class PizzaFactory {
    public static Pizza createPizza(String type) {
        Pizza pizza = null;
        if (type.equals("vegetable")) {
            pizza = new VegetablePizza();
        } else if(type.equals("cheese")) {
            pizza = new CheesePizza();
        } else {
            throw new RuntimeException("no pizza type found");
        }
        pizza.addIngredients();
        pizza.bakePizza();
        return pizza;
    }
}
