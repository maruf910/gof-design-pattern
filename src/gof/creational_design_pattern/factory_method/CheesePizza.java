package gof.creational_design_pattern.factory_method;

public class CheesePizza implements Pizza{

    @Override
    public void addIngredients() {
        System.out.println("Adding ingredients for cheese pizza");
    }

    @Override
    public void bakePizza() {
        System.out.println("Bake cheese pizza");
    }
}
