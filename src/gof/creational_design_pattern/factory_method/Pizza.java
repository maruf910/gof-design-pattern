package gof.creational_design_pattern.factory_method;

public interface Pizza {
    public void addIngredients();
    public void bakePizza();
}
