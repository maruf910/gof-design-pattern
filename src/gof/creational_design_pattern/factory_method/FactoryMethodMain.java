package gof.creational_design_pattern.factory_method;

public class FactoryMethodMain {
    public static void main(String[] args) {
        Pizza vegPizza = PizzaFactory.createPizza("vegetable");
        Pizza cheesePizza = PizzaFactory.createPizza("cheese");
    }
}
