package gof.creational_design_pattern.singleton;

public class SingletonClass {
    // create private final instance
    private static final SingletonClass INSTANCE = new SingletonClass();

    // make the constructor private
    private SingletonClass() {};

    private int val = 5;
    public static SingletonClass getInstance() {
        // every time same instance will be return
        return INSTANCE;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }
}
