package gof.creational_design_pattern.singleton;

public class SingletonDesignPattern {

    public static void main(String[] args) {
        SingletonClass singletonClass = SingletonClass.getInstance();
        System.out.println(singletonClass.getVal());
        singletonClass.setVal(6);
        System.out.println(singletonClass.getVal());
        SingletonClass singletonClass2 = SingletonClass.getInstance();
        System.out.println(singletonClass2.getVal());
    }
}

//https://stackoverflow.com/questions/228164/on-design-patterns-when-should-i-use-the-singleton